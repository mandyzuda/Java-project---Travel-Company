package projeto;

import java.util.List;
public class Administrador extends Funcionario {
	

	public Administrador(String login, String senha) {
		super(login, senha);
	}

	public void novoPassag(List<Passageiro> lstPassageiros) throws Exception {
		System.out.println("Funcionario Administrador nao tem acesso a esta funcao.");
	}

	public void vende(List<Onibus> lstOnibus, List<Passageiro> lstPassageiros,
			List<Viagem> lstViagens) throws Exception {
		System.out.println("Funcionario Administrador nao tem acesso a esta funcao.");
	}


	public void trocaPolt(List<Viagem> lstViagens, List<Onibus> lstOnibus,
			List<Passageiro> lstPassageiros) throws Exception {
		System.out.println("Funcionario Administrador nao tem acesso a esta funcao.");
	}

	public void trocaViagem(List<Viagem> lstViagens, List<Onibus> lstOnibus,
			List<Passageiro> lstPassageiros) throws Exception {
		System.out.println("Funcionario Administrador nao tem acesso a esta funcao.");
	}

	
	public void cancela(List<Viagem> lstViagens, List<Onibus> lstOnibus,
			List<Passageiro> lstPassageiros) throws Exception {
		System.out.println("Funcionario Administrador nao tem acesso a esta funcao.");
	}

}
