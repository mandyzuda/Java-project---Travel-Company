package projeto;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class CampBras {
	public static void main(String[] args) throws Exception {
		List<Passageiro> lstPassageiros = new ArrayList<Passageiro>();
		List<Motorista> lstMotoristas = new ArrayList<Motorista>();
		List<Trajeto> lstTrajetos = new ArrayList<Trajeto>();
		List<Viagem> lstViagens = new ArrayList<Viagem>();
		List<Onibus> lstOnibus = new ArrayList<Onibus>();
		Funcionario diretor = new Funcionario("diretor", "dir1234");
		Operador operador = new Operador("operador", "op1234");
		Administrador administrador = new Administrador("administrador", "adm1234");

		lstMotoristas.add(new Motorista("Joao Silva", 54254521L,
				"12, 02, 2013", 2));
		lstMotoristas.add(new Motorista("Andre Costa", 52154825L,
				"10, 12, 2013", 1));
		lstOnibus.add(new Onibus("Mercedes", "OF1721", "Preto", 2012,
				"20/02/2012", 20, "20/02/2016", false, true, true));
		lstOnibus.add(new Onibus("Mercedes", "OF1721", "Preto", 2013,
				"20/02/2012", 20, "20/02/2016", true, true, true));
		lstTrajetos.add(new Trajeto("Campinas", "Brasilia", 917, 10, 5, 35));
		lstTrajetos.add(new Trajeto("Brasilia", "Campinas", 917, 10, 5, 35));
		lstViagens.add(new Viagem("CB-S-0830", lstOnibus.get(0), lstTrajetos
				.get(0), lstMotoristas.get(0), 86, 27, 05, 2015, 8, 30, 20, 0));
		lstViagens
				.add(new Viagem("BC-S-1230", lstOnibus.get(0), lstTrajetos
						.get(1), lstMotoristas.get(0), 86, 27, 05, 2015, 12,
						30, 20, 0));
		lstViagens
				.add(new Viagem("CB-S-1530", lstOnibus.get(1), lstTrajetos
						.get(0), lstMotoristas.get(1), 86, 27, 05, 2015, 15,
						30, 20, 0));
		lstViagens
				.add(new Viagem("BC-S-2030", lstOnibus.get(1), lstTrajetos
						.get(1), lstMotoristas.get(1), 86, 27, 05, 2015, 20,
						30, 20, 0));
		lstPassageiros.add(new Passageiro("Joaquim Andrade", 56245214521L, 1,
				"Cps - Bsl, 0830, 21/02/2015"));
		lstPassageiros.add(new Passageiro("Maria Andrade", 23545125845L, 1,
				"Cps - Bsl, 0830, 21/02/2015"));
		lstPassageiros.add(new Passageiro("Joana Silva", 24354265212L, 0, "-"));
		
		lstViagens.get(0).getBus().guarde_passageiro(lstPassageiros.get(0), 2);
		
		String[] opcoes = { "Cadastrar passageiro", "Vender passagem",
				"Trocar passageiro de poltrona", "Trocar passageiro de viagem",
				"Cancelar venda", "Cadastrar motorista", "Alterar motorista",
				"Cadastrar onibus", "Alterar onibus", "Cadastrar viagem",
				"Alterar viagem", "Cadastrar trajeto", "Alterar trajeto",
				"SAIR" };
		System.out.println("***************************************************");
		System.out.println("\n**************** CAMPBRAS VIAGENS *****************");
		System.out.println("\n***************************************************");
		
		JTextField username = new JTextField();
		JTextField password = new JPasswordField();
		Object[] message = {
		    "Login:", username,
		    "Password:", password
		};

		int login = JOptionPane.showConfirmDialog(null, message, "Login", JOptionPane.OK_CANCEL_OPTION);
		if (login == JOptionPane.OK_OPTION) {
		    if (username.getText().equals(diretor.getLogin()) && password.getText().equals(diretor.getSenha())) {
		        System.out.println("Login de DIRETOR efetuado com sucesso");
		        while (true) {
					String option = (String) JOptionPane.showInputDialog(null,
							"Selecione uma opcao: ", "SISTEMA CAMPBRAS",
							JOptionPane.QUESTION_MESSAGE, null, opcoes, opcoes[0]);
					
					switch (option) {
					case "Cadastrar passageiro":
						diretor.novoPassag(lstPassageiros);
						for(int i=0; i<lstPassageiros.size(); i++)
						{
							System.out.println(lstPassageiros.get(i));
						}
						break;
					case "Vender passagem":
						diretor.vende(lstOnibus, lstPassageiros, lstViagens);
						break;
					case "Trocar passageiro de poltrona":
						diretor.trocaPolt(lstViagens, lstOnibus, lstPassageiros);
						break;
					case "Trocar passageiro de viagem":
						diretor.trocaViagem(lstViagens, lstOnibus, lstPassageiros);
						break;
					case "Cancelar venda":
						diretor.cancela(lstViagens, lstOnibus, lstPassageiros);
						break;
					case "Cadastrar motorista":
						diretor.novoMotorista(lstMotoristas);
						break;
					case "Alterar motorista":
						diretor.editaMotorista(lstMotoristas);
						break;
					case "Cadastrar onibus":
						diretor.novoOnibus(lstOnibus);
						break;
					case "Alterar onibus":
						diretor.editaOnibus(lstOnibus);
						break;
					case "Cadastrar viagem":
						diretor.novaViagem(lstViagens, lstTrajetos, lstMotoristas,
								lstOnibus);
						break;
					case "Alterar viagem":
						diretor.editaViagem(lstViagens, lstTrajetos, lstMotoristas, lstOnibus);
						break;
					case "Cadastrar trajeto":
						diretor.novoTrajeto(lstTrajetos);
						break;
					case "Alterar trajeto":
						diretor.editaTrajeto(lstTrajetos);
						break;
					case "SAIR":
						System.exit(0);
					}

				}

		    } if (username.getText().equals(operador.getLogin()) && password.getText().equals(operador.getSenha())) {
		    	System.out.println("Login de OPERADOR efetuado com sucesso");
		        while (true) {
					String option = (String) JOptionPane.showInputDialog(null,
							"Selecione uma opcao: ", "SISTEMA CAMPBRAS",
							JOptionPane.QUESTION_MESSAGE, null, opcoes, opcoes[0]);
					switch (option) {
					case "Cadastrar passageiro":
						operador.novoPassag(lstPassageiros);
						for(int i=0; i<lstPassageiros.size(); i++)
						{
							System.out.println(lstPassageiros.get(i));
						}
						break;
					case "Vender passagem":
						operador.vende(lstOnibus, lstPassageiros, lstViagens);
						break;
					case "Trocar passageiro de poltrona":
						operador.trocaPolt(lstViagens, lstOnibus, lstPassageiros);
						break;
					case "Trocar passageiro de viagem":
						operador.trocaViagem(lstViagens, lstOnibus, lstPassageiros);
						break;
					case "Cancelar venda":
						operador.cancela(lstViagens, lstOnibus, lstPassageiros);
						break;
					case "Cadastrar motorista":
						operador.novoMotorista(lstMotoristas);
						break;
					case "Altera motorista":
						operador.editaMotorista(lstMotoristas);
						break;
					case "Cadastrar onibus":
						operador.novoOnibus(lstOnibus);
						break;
					case "Altera onibus":
						operador.editaOnibus(lstOnibus);
						break;
					case "Cadastrar viagem":
						operador.novaViagem(lstViagens, lstTrajetos, lstMotoristas,
								lstOnibus);
						break;
					case "Altera viagem":
						operador.editaViagem(lstViagens, lstTrajetos, lstMotoristas, lstOnibus);
						break;
					case "Cadastrar trajeto":
						operador.novoTrajeto(lstTrajetos);
						break;
					case "Altera trajeto":
						operador.editaTrajeto(lstTrajetos);
						break;
					case "SAIR":
						System.exit(0);
					}

				}

		    }  if (username.getText().equals(administrador.getLogin()) && password.getText().equals(administrador.getSenha())) {
		    	System.out.println("Login de ADMINISTRADOR efetuado com sucesso");
		        while (true) {
					String option = (String) JOptionPane.showInputDialog(null,
							"Selecione uma opcao: ", "SISTEMA CAMPBRAS",
							JOptionPane.QUESTION_MESSAGE, null, opcoes, opcoes[0]);
					switch (option) {
					case "Cadastrar passageiro":
						administrador.novoPassag(lstPassageiros);
						for(int i=0; i<lstPassageiros.size(); i++)
						{
							System.out.println(lstPassageiros.get(i));
						}
						break;
					case "Vender passagem":
						administrador.vende(lstOnibus, lstPassageiros, lstViagens);
						break;
					case "Trocar passageiro de poltrona":
						administrador.trocaPolt(lstViagens, lstOnibus, lstPassageiros);
						break;
					case "Trocar passageiro de viagem":
						administrador.trocaViagem(lstViagens, lstOnibus, lstPassageiros);
						break;
					case "Cancelar venda":
						administrador.cancela(lstViagens, lstOnibus, lstPassageiros);
						break;
					case "Cadastrar motorista":
						administrador.novoMotorista(lstMotoristas);
						break;
					case "Altera motorista":
						administrador.editaMotorista(lstMotoristas);
						break;
					case "Cadastrar onibus":
						administrador.novoOnibus(lstOnibus);
						break;
					case "Altera onibus":
						administrador.editaOnibus(lstOnibus);
						break;
					case "Cadastrar viagem":
						administrador.novaViagem(lstViagens, lstTrajetos, lstMotoristas,
								lstOnibus);
						break;
					case "Altera viagem":
						administrador.editaViagem(lstViagens, lstTrajetos, lstMotoristas, lstOnibus);
						break;
					case "Cadastrar trajeto":
						administrador.novoTrajeto(lstTrajetos);
						break;
					case "Altera trajeto":
						administrador.editaTrajeto(lstTrajetos);
						break;
					case "SAIR":
						System.exit(0);
					}

				} 
		    } else if(login == JOptionPane.CANCEL_OPTION) {
		        System.out.println("Login cancelado");
		    }
		} else {
		    System.out.println("Login nao efetuado. Tente novamente.");
		}
		
		
	}
}
