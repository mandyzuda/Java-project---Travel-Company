package projeto;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.util.*;

//sera o funcionario DIRETOR, com acesso a todas as funcionalidades do sistema

public class Funcionario {

	// Funcionalidades operacionais

	private String login;
	private String senha;
	
	public Funcionario(String login, String senha) {
		this.login = login;
		this.senha = senha;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * Novo cadastro de passageiro, adicionando-o a lista de passgeiros do
	 * sistema.
	 * 
	 * @throws Exception
	 */
	public void novoPassag(List<Passageiro> lstPassageiros) throws Exception {
		Passageiro ocp = new Passageiro();
		String nome = JOptionPane.showInputDialog(null,
				"Digite o nome do passageiro: ", "DADOS DO PASSAGEIRO",
				JOptionPane.QUESTION_MESSAGE);
		ocp.setNome(nome);
		String CPF = JOptionPane.showInputDialog(null,
				"Digite o CPF do passageiro: ", "DADOS DO PASSAGEIRO",
				JOptionPane.QUESTION_MESSAGE);
		long CPF2 = Long.parseLong(CPF);
		ocp.setCPF(CPF2);
		String qtd = JOptionPane.showInputDialog(null,
				"Quantidade de viagens do passageiro: ", "DADOS DO PASSAGEIRO",
				JOptionPane.QUESTION_MESSAGE);
		int qtd2 = Integer.parseInt(qtd);
		ocp.setQtd(qtd2);
		if (qtd2 > 0) {
			String prim = JOptionPane.showInputDialog(null,
					"Dados da primeira viagem do passageiro: ",
					"DADOS DO PASSAGEIRO", JOptionPane.QUESTION_MESSAGE);
			ocp.setPrimeira(prim);
		}
		lstPassageiros.add(ocp);
	}

	/**
	 * Mostra uma lista com as viagens existentes para o funcionario selecionar
	 * a escolha do cliente, retornando o index da lista se existir ou -1 se nao
	 * existir
	 * 
	 * @param lstViagens
	 * @return
	 */
	public int seleciona(List<Viagem> lstViagens) {
		String[] viagens = new String[lstViagens.size()];
		for (int i = 0; i < lstViagens.size(); i++) {
			viagens[i] = lstViagens.get(i).getId();
		}

		JFrame frame = new JFrame();

		String viag = (String) JOptionPane.showInputDialog(frame,
				"Selecione uma viagem: ", "Escolha viagem",
				JOptionPane.QUESTION_MESSAGE, null, viagens, viagens[0]);
		System.out.println("Viagem selecionada: " + viag);
		int indexv = 0;

		for (int i = 0; i < viagens.length; i++) {
			if (viag == viagens[i])
				indexv = i;
		}
		return indexv;
	}

	/**
	 * Verifica se o passageiro tem cadastro ou nao
	 * 
	 * @param lstPassageiros
	 * @return
	 */
	public int verificaPassageiro(List<Passageiro> lstPassageiros) {
		String nome = JOptionPane.showInputDialog(null,
				"Digite o nome do passageiro cadastrado: ",
				"Procure passageiro cadastrado", JOptionPane.QUESTION_MESSAGE);
		System.out.println("Passageiro digitado: " + nome);
		for (Passageiro p : lstPassageiros) {
			if (p.getNome().equals(nome))
				System.out.println("Passageiro digitado: " + p.getNome());
				return lstPassageiros.indexOf(p);
		}
		JOptionPane.showMessageDialog(null, "Passageiro nao tem cadastro.",
				"Passageiro nao encontrado", JOptionPane.ERROR_MESSAGE);
		return -1;
	}

	/**
	 * Vender passagem para cliente. Deve escolher poltrona ou deixar o sistema
	 * reservar a primeira disponivel. Verifica se o passageiro tem cadastro, se
	 * nao houver, cria um cadastro. Oferece outra compra se o passageiro
	 * estiver interessado
	 * 
	 * @param lstOnibus
	 * @param lstPassageiros
	 * @param lstViagens
	 * @throws Exception
	 */
	public void vende(List<Onibus> lstOnibus, List<Passageiro> lstPassageiros,
			List<Viagem> lstViagens) throws Exception {
		// Para vender passagem, verificar a poltrona escolhida e se o
		// passageiro tem cadastro
		for (int i = 0; i < lstViagens.size(); i++) {
			System.out.println(lstViagens.get(i));
		} 
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		int index = seleciona(lstViagens);
		System.out.println("\n\tVender passagem\n\t");
		if (lstViagens.get(index).getBus().estaLotado() == true) {
			System.out.println("Viagem lotada!\n");
		} else {
			String poltrona = JOptionPane
					.showInputDialog(
							null,
							"Digite qual poltrona voce deseja comprar, ou digite 0 caso nao queira escolher uma: ",
							"Vender passagem", JOptionPane.QUESTION_MESSAGE);
			int polt = Integer.parseInt(poltrona);
			int p = verificaPassageiro(lstPassageiros);
			if (polt == 0) {
				if (p == -1)
					lstViagens.get(index).getBus().guarde_passageiro();
				else
					lstViagens.get(index).getBus()
							.guarde_passageiro(lstPassageiros.get(p), polt);
			} else {
				while (lstViagens.get(index).getBus().verificaPoltrona(polt) != true) {
					poltrona = JOptionPane
							.showInputDialog(
									null,
									"Poltrona requisitada esta ocupada. Digite a nova poltrona para realocar passageiro: ",
									"Nova poltrona",
									JOptionPane.QUESTION_MESSAGE);
					polt = Integer.parseInt(poltrona);
				}
				if(p == -1) {
					lstViagens.get(index).getBus().guarde_passageiro();
				}
				else {
					lstViagens.get(index).getBus()
						.guarde_passageiro(lstPassageiros.get(p), polt);
				}
			}
			JOptionPane.showMessageDialog(null,
					"Passagem comprada com sucesso!", "VENDA EFETUADA",
					JOptionPane.INFORMATION_MESSAGE);
			lstViagens.get(index).setQtdPoltPreench(lstViagens.get(index).getQtdPoltPreench() + 1);
			lstViagens.get(index).setQtdPoltVazia(lstViagens.get(index).getQtdPoltVazia() - 1);
			for (int i = 0; i < lstViagens.size(); i++) {
				System.out.println(lstViagens.get(i));
			} 
			System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
			int reply = JOptionPane.showConfirmDialog(null,
					"Vender outra passagem?", "Vender passagem",
					JOptionPane.YES_NO_OPTION);
			if (reply == JOptionPane.YES_OPTION) {
				vende(lstOnibus, lstPassageiros, lstViagens);
			}

		}

	}

	/**
	 * Troca poltrona do passageiro, pede nova poltrona, a atual e verifica se o
	 * passageiro esta de fato na poltrona. Caso esteja, adiciona o passageiro.
	 * Caso nao, aponta o erro. Caso esteja lotado, avisa.
	 * 
	 * @param lstViagens
	 * @param lstOnibus
	 * @param lstPassageiros
	 * @throws Exception
	 */
	public void trocaPolt(List<Viagem> lstViagens, List<Onibus> lstOnibus,
			List<Passageiro> lstPassageiros) throws Exception {
		for (int i = 0; i < lstViagens.size(); i++) {
			System.out.println(lstViagens.get(i));
		} 
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		int pass = verificaPassageiro(lstPassageiros);
		int index = seleciona(lstViagens); // qual viagem o passageiro comprou
		if (lstViagens.get(index).getBus().estaLotado()) {
			JOptionPane
					.showMessageDialog(
							null,
							"Onibus desta viagem esta lotada, nao sera possivel alterar a poltrona do passageiro",
							"ONIBUS LOTADO", JOptionPane.ERROR_MESSAGE);
		} else if (pass != -1) {
			String p2 = JOptionPane.showInputDialog(null,
					"Digite a nova poltrona para realocar passageiro: ",
					"Trocar poltrona", JOptionPane.QUESTION_MESSAGE);
			int polt = Integer.parseInt(p2);
			while (lstViagens.get(index).getBus().verificaPoltrona(polt) != true) {
				p2 = JOptionPane
						.showInputDialog(
								null,
								"Poltrona requisitada esta ocupada. Digite a nova poltrona para realocar passageiro: ",
								"Nova poltrona", JOptionPane.QUESTION_MESSAGE);
				polt = Integer.parseInt(p2);
			}

			lstViagens.get(index).getBus()
					.remova(lstPassageiros.get(pass).getNome());
			lstViagens.get(index).getBus()
					.guarde_passageiro(lstPassageiros.get(pass), polt);
			System.out.println("Troca de poltrona efetuada com sucesso!");
			for (int i = 0; i < lstViagens.size(); i++) {
				System.out.println(lstViagens.get(i));
			} 

		} else
			JOptionPane.showMessageDialog(null,
					"Passageiro não encontrado na lista de passageiros.\n",
					"PASSAGEIRO NAO ENCONTRADO", JOptionPane.ERROR_MESSAGE);
	}

	public void trocaViagem(List<Viagem> lstViagens, List<Onibus> lstOnibus,
			List<Passageiro> lstPassageiros) throws Exception {
		int pass = verificaPassageiro(lstPassageiros);
		JOptionPane.showMessageDialog(null,
				"Selecione a viagem atual do passageiro!", "VIAGEM ATUAL",
				JOptionPane.INFORMATION_MESSAGE);
		int index = seleciona(lstViagens);
		String nome = lstPassageiros.get(pass).getNome();
		if (lstViagens.get(index).getBus().onde(nome) != -1) {
			JOptionPane
					.showMessageDialog(
							null,
							"Selecione a viagem para qual o passageiro gostaria de ser alterado!",
							"NOVA VIAGEM", JOptionPane.INFORMATION_MESSAGE);
			int index2 = seleciona(lstViagens);
			if (lstViagens.get(index2).getBus().estaLotado()) {
				JOptionPane
						.showMessageDialog(
								null,
								"Onibus desta viagem esta lotada, nao sera possivel alterar o passageiro",
								"ONIBUS LOTADO", JOptionPane.ERROR_MESSAGE);
			} else {
				int p = lstViagens.get(index).getBus().kd_passageiro(nome);
				if (lstViagens.get(index2).getBus().verificaPoltrona(p)) {
					// verificar se poltrona que passageiro ocupava na viagem
					// anterior esta ocupada na nova viagem
					lstViagens.get(index2).getBus()
							.guarde_passageiro(lstPassageiros.get(pass), p);
				}
				String p2 = JOptionPane
						.showInputDialog(
								null,
								"Poltrona que passageiro ocupava na viagem anterior esta ocupada na nova viagem. Digite a nova poltrona para realocar passageiro: ",
								"Nova poltrona", JOptionPane.QUESTION_MESSAGE);
				int polt = Integer.parseInt(p2);
				while (lstViagens.get(index2).getBus().verificaPoltrona(polt) != true) {
					p2 = JOptionPane
							.showInputDialog(
									null,
									"Poltrona requisitada esta ocupada. Digite a nova poltrona para realocar passageiro: ",
									"Nova poltrona",
									JOptionPane.QUESTION_MESSAGE);
					polt = Integer.parseInt(p2);
				}
				lstViagens.get(index2).getBus()
						.guarde_passageiro(lstPassageiros.get(pass), polt);
				lstViagens.get(index).getBus().remova(nome);
				System.out.println("Passageiro alterado de poltrona com sucesso!");
				lstViagens.get(index).setQtdPoltPreench(lstViagens.get(index).getQtdPoltPreench() - 1);
				lstViagens.get(index).setQtdPoltVazia(lstViagens.get(index).getQtdPoltVazia() + 1);
				lstViagens.get(index2).setQtdPoltPreench(lstViagens.get(index2).getQtdPoltPreench() + 1);
				lstViagens.get(index2).setQtdPoltVazia(lstViagens.get(index2).getQtdPoltVazia() - 1);
				
				for (int i = 0; i < lstViagens.size(); i++) {
					System.out.println(lstViagens.get(i));
				} 
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
			}

		}
	}

	/**
	 * Cancela venda de passagem por desistencia do cliente. Pede o nome,
	 * verifica se o passageiro esta no onibus, confirma se realmente quer
	 * cancelar e o retira caso sim.
	 * 
	 * @param lstViagens
	 * @param lstOnibus
	 * @param lstPassageiros
	 * @throws Exception
	 */
	public void cancela(List<Viagem> lstViagens, List<Onibus> lstOnibus,
			List<Passageiro> lstPassageiros) throws Exception {
		for (int i = 0; i < lstViagens.size(); i++) {
			System.out.println(lstViagens.get(i));
		} 
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		int pass = verificaPassageiro(lstPassageiros);
		JOptionPane
				.showMessageDialog(
						null,
						"Para cancelar passagem, selecione a viagem atual do passageiro!",
						"CANCELAR PASSAGEM POR DESISTENCIA",
						JOptionPane.WARNING_MESSAGE);
		int index = seleciona(lstViagens);
		String nome = lstPassageiros.get(pass).getNome();
		int reply = JOptionPane.showConfirmDialog(null,
				"Tem certeza que deseja cancelar esta passagem?",
				"Cancelar passagem por desistencia", JOptionPane.YES_NO_OPTION);
		if (reply == JOptionPane.YES_OPTION) {
			lstViagens.get(index).getBus().remova(nome);
			JOptionPane.showMessageDialog(null,
					"Cancelamento efetuado com sucesso!",
					"Cancelamento efetuado", JOptionPane.INFORMATION_MESSAGE);
			lstViagens.get(index).setQtdPoltPreench(lstViagens.get(index).getQtdPoltPreench() - 1);
			lstViagens.get(index).setQtdPoltVazia(lstViagens.get(index).getQtdPoltVazia() + 1);
			for (int i = 0; i < lstViagens.size(); i++) {
				System.out.println(lstViagens.get(i));
			} 
			System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		} else {
			System.out.println("Passagem nao cancelada.\n");

		}
	}

	// Funcionalidades Administrativas
	// Interagir, editar e cadastrar Motorista, Onibus, Viagem, Trajeto
	public void novoMotorista(List<Motorista> lstMotoristas) throws Exception {
		Motorista m = new Motorista();

		Scanner objLeitor = new Scanner(System.in);

		System.out
				.println("\n\n \tCADASTRAR NOVO MOTORISTA\n\n \tNome do motorista: ");
		m.setNome(objLeitor.nextLine());
		System.out.println("\tRG: ");
		m.setRg(objLeitor.nextLong());
		System.out
				.println("\tData de contratacao do motorista, no formato dia/mes/ano: ");
		m.setDataCont(objLeitor.nextLine());
		objLeitor.nextLine();
		System.out.println("\tAnos de experiencia do motorista: ");
		m.setAnoExp(objLeitor.nextInt());
		lstMotoristas.add(m);
		System.out.println("Motorista cadastrado com sucesso!");
		objLeitor.close();
		for (int i = 0; i < lstMotoristas.size(); i++) {
			System.out.println(i + " . " + lstMotoristas.get(i));
		}
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}

	public void editaMotorista(List<Motorista> lstMotoristas) throws Exception {
		for (int i = 0; i < lstMotoristas.size(); i++) {
			System.out.println(i + " . " + lstMotoristas.get(i));
		}
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		System.out
				.println("Qual motorista voce gostaria de editar? Utilize o numero inicial. ");
		Scanner objLeitor = new Scanner(System.in);
		int op = objLeitor.nextInt();
		int edit = 1;
		while (edit != 0) {
			System.out
					.println("O que voce gostaria de editar? Utilize o numero inicial da lista: ");
			System.out
					.println("\n\n \t1. Nome\n \t2.RG\n \t3.Data de contratacao\n \t4. Anos de experiencia\n \t0. Terminar edicao");
			edit = objLeitor.nextInt();
			if (edit == 1) {
				objLeitor.nextLine();
				System.out.println("\n\n \tDigite o novo nome: ");
				String nome = objLeitor.nextLine();
				lstMotoristas.get(op).setNome(nome);
			} else if (edit == 2) {
				System.out.println("\n\n \tDigite o novo RG: ");
				Long rg = objLeitor.nextLong();
				lstMotoristas.get(op).setRg(rg);
			} else if (edit == 3) {

				objLeitor.nextLine();
				System.out
						.println("\n\n \tDigite a nova data de contratacao: ");
				String data = objLeitor.nextLine();
				lstMotoristas.get(op).setDataCont(data);
			} else if (edit == 4) {
				System.out
						.println("\n\n \tDigite quantidade de anos de experiencia: ");
				int exp = objLeitor.nextInt();
				lstMotoristas.get(op).setAnoExp(exp);
			} else if(edit == 0) {
				break;
			}	else {
				System.out.println("\n\n \tOpcao invalida.");
			}
		}
		System.out.println("Motorista editado com sucesso!");
		objLeitor.close();
		for (int i = 0; i < lstMotoristas.size(); i++) {
			System.out.println(i + " . " + lstMotoristas.get(i));
		}
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}

	public void novoOnibus(List<Onibus> lstOnibus) throws Exception {
		
		Scanner objLeitor = new Scanner(System.in);
		System.out
				.println("\n\n \tCADASTRAR NOVO ONIBUS\n\n \tQuantidade de poltronas: ");
		int qtd = objLeitor.nextInt();
		Onibus o = new Onibus(qtd);

		System.out.println("\tMarca: ");
		o.setMarca(objLeitor.nextLine());
		System.out.println("\tModelo: ");
		o.setModelo(objLeitor.nextLine());
		System.out.println("\tCor: ");
		o.setCor(objLeitor.nextLine());
		System.out.println("\tAno de fabricacao: ");
		o.setAnofab(objLeitor.nextInt());
		System.out.println("\tData de aquisicao (dia/mes/ano): ");
		o.setDataAquisicao(objLeitor.nextLine());
		System.out.println("\tData de revisao (dia/mes/ano): ");
		o.setDataRevisao(objLeitor.nextLine());
		System.out.println("\tTem TV? ");
		o.setTv(objLeitor.nextBoolean());
		System.out.println("\tTem Wifi? ");
		o.setWifi(objLeitor.nextBoolean());
		System.out.println("\tTem banheiro? ");
		o.setWc(objLeitor.nextBoolean());
		o.initPoltronas();

		objLeitor.close();
		lstOnibus.add(o);
		System.out.println("Onibus cadastrado com sucesso!");
		for (int i = 0; i < lstOnibus.size(); i++) {
			System.out.println(i + " . " + lstOnibus.get(i));
		}
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}

	public void editaOnibus(List <Onibus> lstOnibus) throws Exception {
		for (int i = 0; i < lstOnibus.size(); i++) {
			System.out.println(i + " . " + lstOnibus.get(i));
		}
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		System.out
				.println("Qual onibus voce gostaria de editar? Utilize o numero inicial. ");
		Scanner objLeitor = new Scanner(System.in);
		int op = objLeitor.nextInt();
		int edit = 1;
		while (edit != 0) {
			System.out
					.println("O que voce gostaria de editar? Utilize o numero inicial da lista: ");
			System.out
					.println("\n\n \t1. Marca\n \t2. Modelo\n \t3. Cor\n \t4. Ano de Fabricacao\n \t5. Data de aquisicao\n \t6. Quantidade de poltronas\n \t7. Alterar TV\n \t8. Alterar Wifi \n \t9. Alterar banheiro\n \t0. Terminar edicao");
			edit = objLeitor.nextInt();
			if (edit == 1) {

				objLeitor.nextLine();
				System.out.println("\n\n \tDigite a nova marca: ");
				String marca = objLeitor.nextLine();
				lstOnibus.get(op).setMarca(marca);
			} else if (edit == 2) {

				objLeitor.nextLine();
				System.out.println("\n\n \tDigite o novo modelo: ");
				String modelo = objLeitor.nextLine();
				lstOnibus.get(op).setModelo(modelo);
			} else if (edit == 3) {

				objLeitor.nextLine();
				System.out
						.println("\n\n \tDigite a nova cor: ");
				String cor = objLeitor.nextLine();
				lstOnibus.get(op).setCor(cor);
			} else if (edit == 4) {
				System.out
					.println("\n\n \tDigite o ano de fabricacao: ");
				int ano = objLeitor.nextInt();
				lstOnibus.get(op).setAnofab(ano);
			}else if (edit == 5) {

				objLeitor.nextLine();
				System.out
						.println("\n\n \tDigite a data de aquisicao: ");
				String data = objLeitor.nextLine();
				lstOnibus.get(op).setDataAquisicao(data);
			} else if (edit == 6) {
				System.out
					.println("\n\n \tDigite a quantidade de poltronas: ");
				int qtd = objLeitor.nextInt();
				lstOnibus.get(op).setQtd(qtd);
			} else if (edit == 7) {
				System.out
					.println("\n\n \tAlterar TV: ");
				boolean tv = objLeitor.nextBoolean();
				lstOnibus.get(op).setTv(tv);
			} else if (edit == 8) {
				System.out
					.println("\n\n \tAlterar Wifi: ");
				boolean wifi = objLeitor.nextBoolean();
				lstOnibus.get(op).setWifi(wifi);
			}  else if (edit == 9) {
				System.out
					.println("\n\n \tAlterar banheiro: ");
				boolean wc = objLeitor.nextBoolean();
				lstOnibus.get(op).setWc(wc);
			} else if(edit == 0) {
				break;
			}	else {
				System.out.println("\n\n \tOpcao invalida.");
			}
		}

		objLeitor.close();
		System.out.println("Onibus editado com sucesso!");
		for (int i = 0; i < lstOnibus.size(); i++) {
			System.out.println(i + " . " + lstOnibus.get(i));
		}
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}

	public void novaViagem(List<Viagem> lstViagens, List<Trajeto> lstTrajetos,
			List<Motorista> lstMotoristas, List<Onibus> lstOnibus)
			throws Exception {
		Viagem v = new Viagem();
		Scanner objLeitor = new Scanner(System.in);

		System.out
				.println("\n\n \tCADASTRAR NOVA VIAGEM\n\n \tDigite o DIA da viagem (numero): ");
		v.setDia(objLeitor.nextInt());
		System.out.println("\tDigite o MES da viagem (numero): ");
		v.setMes(objLeitor.nextInt());
		System.out.println("\tDigite o ANO da viagem (numero): ");
		v.setAno(objLeitor.nextInt());
		System.out.println("\tDigite a HORA da viagem (numero): ");
		int hora = objLeitor.nextInt();
		v.setHora(hora);
		System.out.println("\tDigite os MINUTOS da viagem (numero): ");
		int minuto = objLeitor.nextInt();
		v.setMinuto(minuto);
		System.out.println("\tDigite a TARIFA da passagem da viagem: ");
		v.setTarifa(objLeitor.nextFloat());
		
		for (int i = 0; i < lstOnibus.size(); i++) {
			System.out.println(i + "." + lstOnibus.get(i));
		}
		System.out
			.println("Qual onibus fara esta viagem? Utilize o numero inicial. ");
		int op = objLeitor.nextInt();
		v.setBus(lstOnibus.get(op));
		
		for (int i = 0; i < lstTrajetos.size(); i++) {
			System.out.println(i + "." + lstTrajetos.get(i));
		}
		System.out
			.println("Qual sera o trajeto desta viagem? Utilize o numero inicial. ");
		int op2 = objLeitor.nextInt();
		v.setTraj(lstTrajetos.get(op2));
		
		for (int i = 0; i < lstMotoristas.size(); i++) {
			System.out.println(i + "." + lstMotoristas.get(i));
		}
		System.out
			.println("Qual sera o motorista desta viagem? Utilize o numero inicial. ");
		int op3 = objLeitor.nextInt();
		v.setMot(lstMotoristas.get(op3));
		
		System.out
			.println("A viagem sera durante a semana (digite S) ou no final de smana (digite F)? ");
		char qnd = objLeitor.next().charAt(0);
		String id = lstTrajetos.get(op2).getCidDestino().charAt(0) + lstTrajetos.get(op2).getCidOrigem().charAt(0) + "-" + qnd + "-" + hora + minuto;
		
		v.setId(id);
		v.setQtdPoltVazia(lstOnibus.get(op).getQtd());
		v.setQtdPoltPreench(0);
		objLeitor.close();
		lstViagens.add(v);
		System.out.println("Viagem cadastrada com sucesso!");
		for (int i = 0; i < lstViagens.size(); i++) {
			System.out.println(i + "." + lstViagens.get(i));
		}
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}

	public void editaViagem(List<Viagem> lstViagens, List<Trajeto> lstTrajetos,
			List<Motorista> lstMotoristas, List<Onibus> lstOnibus) throws Exception {
		for (int i = 0; i < lstViagens.size(); i++) {
			System.out.println(i + "." + lstViagens.get(i));
		}
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		System.out
				.println("Qual viagem voce gostaria de editar? Utilize o numero inicial. ");
		Scanner objLeitor = new Scanner(System.in);
		int op = objLeitor.nextInt();
		int edit = 1;
		
		while (edit != 0) {
			System.out
					.println("O que voce gostaria de editar? Utilize o numero inicial da lista: ");
			System.out
					.println("\n\n \t1. Tarifa\n \t2. Dia\n \t3. Mes\n \t4. Ano \n \t5. Hora\n \t6. Minuto\n \t7. Onibus\n \t8. Trajeto\n \t9. Motorista\n \t0. Terminar edicao");
			edit = objLeitor.nextInt();
			if (edit == 1) {
				System.out.println("\n\n \tDigite a nova tarifa: ");
				float tar = objLeitor.nextFloat();
				lstViagens.get(op).setTarifa(tar);
			} else if (edit == 2) {
				System.out.println("\n\n \tDigite o novo dia (d): ");
				int d = objLeitor.nextInt();
				lstViagens.get(op).setDia(d);
			} else if (edit == 3) {
				System.out
						.println("\n\n \tDigite o novo mes (m): ");
				int m = objLeitor.nextInt();
				lstViagens.get(op).setMes(m);
			} else if (edit == 4) {
				System.out
					.println("\n\n \tDigite o novo ano (aaaa): ");
				int ano = objLeitor.nextInt();
				lstViagens.get(op).setAno(ano);
			}else if (edit == 5) {
				System.out
						.println("\n\n \tDigite a hora (HH): ");
				int h = objLeitor.nextInt();
				lstViagens.get(op).setHora(h);
			} else if (edit == 6) {
				System.out
					.println("\n\n \tDigite os minutos (mm): ");
				int m = objLeitor.nextInt();
				lstViagens.get(op).setMinuto(m);
			} else if (edit == 7) {
				System.out
					.println("\n\n \tAlterar onibus: ");
				for (int i = 0; i < lstOnibus.size(); i++) {
					System.out.println(i + "." + lstOnibus.get(i));
				}
				System.out
					.println("Qual onibus fara esta viagem? Utilize o numero inicial. ");
				int b = objLeitor.nextInt();
				lstViagens.get(op).setBus(lstOnibus.get(b));
			} else if (edit == 8) {
				System.out
					.println("\n\n \tAlterar Trajeto: ");
				for (int i = 0; i < lstTrajetos.size(); i++) {
					System.out.println(i + "." + lstTrajetos.get(i));
				}
				System.out
					.println("Qual sera o trajeto desta viagem? Utilize o numero inicial. ");
				int t = objLeitor.nextInt();
				lstViagens.get(op).setTraj(lstTrajetos.get(t));
			}  else if (edit == 9) {
				System.out
					.println("\n\n \tAlterar Motorista: ");
				for (int i = 0; i < lstMotoristas.size(); i++) {
					System.out.println(i + "." + lstMotoristas.get(i));
				}
				System.out
					.println("Qual sera o motorista desta viagem? Utilize o numero inicial. ");
				int m = objLeitor.nextInt();
				lstViagens.get(op).setMot(lstMotoristas.get(m));
			} else if(edit == 0) {
				break;
			}	else {
				System.out.println("\n\n \tOpcao invalida.");
			}
			for (int i = 0; i < lstViagens.size(); i++) {
				System.out.println(i + "." + lstViagens.get(i));
			}
			System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		}

		objLeitor.close();
		System.out.println("Viagem editada com sucesso!");
	}

	public void novoTrajeto(List<Trajeto> lstTrajetos) throws Exception {
		Trajeto t = new Trajeto();
		Scanner objLeitor = new Scanner(System.in);
		System.out
				.println("\n\n \tCADASTRAR NOVO TRAJETO\n\n \tCidade de origem: ");
		t.setCidOrigem(objLeitor.nextLine());
		System.out.println("\tCidade destino: ");
		t.setCidDestino(objLeitor.nextLine());
		int i = 0;
		String[] interm = new String[10];
		while(i!=9) {
			System.out.println("\tCidade intermediaria: ");
			interm[i] = objLeitor.nextLine();
			i++;
			System.out.println("\tDigite 0 para parar, qualquer outro numero para continuar: ");
			int stop = objLeitor.nextInt();
			if(stop == 0)
				break;
		}
		t.setCidIntermed(interm);
		System.out.println("\tDistancia entre as cidades: ");
		t.setDist(objLeitor.nextFloat());
		System.out.println("\tTempo de viagem em minutos: ");
		t.setTempo(objLeitor.nextFloat());
		System.out.println("\tQuantidade de pedagios: ");
		t.setQtdPedagios(objLeitor.nextInt());
		System.out.println("\tTotal a pagar em pedagios: ");
		t.setTotalPedagios(objLeitor.nextFloat());

		objLeitor.close();
		lstTrajetos.add(t);
		System.out.println("Trajeto cadastrado com sucesso!");
		for (int i1 = 0; i1 < lstTrajetos.size(); i1++) {
			System.out.println(i1 + "." + lstTrajetos.get(i1));
		}
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}

	public void editaTrajeto(List<Trajeto> lstTrajetos) throws Exception {
		
		for (int i = 0; i < lstTrajetos.size(); i++) {
			System.out.println(i + "." + lstTrajetos.get(i));
		}
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		System.out
				.println("Qual trajeto voce gostaria de editar? Utilize o numero inicial. ");
		Scanner objLeitor = new Scanner(System.in);
		int op = objLeitor.nextInt();
		int edit = 1;
		while (edit != 0) {
			System.out
					.println("O que voce gostaria de editar? Utilize o numero inicial da lista: ");
			System.out
					.println("\n\n \t1. Cidade origem\n \t2.Cidade destino\n \t3.Cidades intermediarias\n \t4. Distancia\n \t5. Tempo\n \t6. Quantidade de pedagios\n \t7. Total de pedagios\n \t0. Terminar edicao");
			edit = objLeitor.nextInt();
			if (edit == 1) {

				objLeitor.nextLine();
				System.out.println("\n\n \tDigite nova cidade origem: ");
				String co = objLeitor.nextLine();
				lstTrajetos.get(op).setCidOrigem(co);
			} else if (edit == 2) {

				objLeitor.nextLine();
				System.out.println("\n\n \tDigite a nova cidade destino: ");
				String cd = objLeitor.nextLine();
				lstTrajetos.get(op).setCidDestino(cd);
			} else if (edit == 3) {
				System.out
						.println("\n\n \tNovas cidades intermediarias: ");
				int i = 0;
				String[] interm = new String[10];
				while(i!=9) {

					objLeitor.nextLine();
					System.out.println("\tCidade intermediaria: ");
					interm[i] = objLeitor.nextLine();
					i++;
					System.out.println("\tDigite 0 para parar, qualquer outro numero para continuar: ");
					int stop = objLeitor.nextInt();
					if(stop == 0)
						break;
				}
				lstTrajetos.get(op).setCidIntermed(interm);
			} else if (edit == 4) {
				System.out
						.println("\n\n \tDigite nova distancia: ");
				float dist = objLeitor.nextFloat();
				lstTrajetos.get(op).setDist(dist);
			} else if (edit == 5) {
				System.out
					.println("\n\n \tDigite novo tempo: ");
				float tempo = objLeitor.nextFloat();
				lstTrajetos.get(op).setTempo(tempo);
			} else if (edit == 6) {
				System.out
					.println("\n\n \tDigite a quantidade de pedagios: ");	
				int qtd = objLeitor.nextInt();
				lstTrajetos.get(op).setQtdPedagios(qtd);
			} else if (edit == 7) {
				System.out
					.println("\n\n \tTotal a pagar em pedagios: ");
				float ped = objLeitor.nextFloat();
				lstTrajetos.get(op).setTotalPedagios(ped);
			}  else if(edit == 0) {
				break;
			}	else {
				System.out.println("\n\n \tOpcao invalida.");
			}
		}

		objLeitor.close();
		System.out.println("Trajeto editado com sucesso!");
		for (int i = 0; i < lstTrajetos.size(); i++) {
			System.out.println(i + "." + lstTrajetos.get(i));
		}
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}

}
