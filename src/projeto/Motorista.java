//Amanda Barboza Braz RA 14432843

package projeto;

/*
 *  O motorista tem:
 o Nome;
 o RG;
 o Data da contratação;
 o Anos de experiência.
 */
public class Motorista {
	private String nome;
	private long rg;
	private String dataCont;
	private int anoExp;

	public Motorista(String nome, long rg, String dataCont, int anoExp)
			throws Exception {
		this.setNome(nome);
		this.setRg(rg);
		this.setDataCont(dataCont);
		this.setAnoExp(anoExp);
	}

	public Motorista() {
		this.nome = null;
		this.rg = 0;
		this.dataCont = null;
		this.anoExp = 0;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) throws Exception {
		if (nome == null)
			throw new Exception("Nome invalido");
		this.nome = nome;
	}

	public long getRg() {
		return rg;
	}

	public void setRg(long rg) throws Exception {
		if ((Long) rg == null)
			throw new Exception("RG invalido");
		this.rg = rg;
	}

	public String getDataCont() {
		return dataCont;
	}

	public void setDataCont(String dataCont) throws Exception {
		if (dataCont == null)
			throw new Exception("Data de contratacao invalido");
		this.dataCont = dataCont;
	}

	public int getAnoExp() {
		return anoExp;
	}

	public void setAnoExp(int anoExp) throws Exception {
		if ((Integer) anoExp == null)
			throw new Exception("Anos de experiencia invalido");
		this.anoExp = anoExp;
	}

	@Override
	public String toString() {
		return "Motorista: [nome=" + nome + ", \nRG=" + rg
				+ ", \ndata de contratação=" + dataCont
				+ ", \nanos de experiencia=" + anoExp + "]";
	}

	@Override
	public int hashCode() {
		int resultado = 1;
		resultado = resultado * 7 + (int) ((this.rg >>> 32) | this.rg);
		resultado = resultado * 7 + this.anoExp;
		resultado = resultado * 7 + this.nome.hashCode();
		resultado = resultado * 7 + this.dataCont.hashCode();
		return resultado;
	}

	@Override
	public boolean equals(Object x) {
		if (this == x)
			return true;
		if (x == null)
			return false;
		if (x instanceof Motorista) {
			Motorista m = (Motorista) x;
			if (this.nome == m.nome && this.rg == m.rg
					&& this.dataCont == m.dataCont && this.anoExp == m.anoExp)
				return true;
		}
		return false;
	}

}
