/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto;

/**
 *
 * @author Amanda B. Braz 14432843
 */

import javax.swing.JOptionPane;

import projeto.Poltrona;

public class Onibus {
	/**
	 * A marca do ônibus.
	 */
	private String marca;
	private String modelo;
	private String cor;
	private int anofab;
	private String dataAquisicao;
	private int qtd;
	private String dataRevisao;
	private boolean tv;
	private boolean wifi;
	private boolean wc; // wc = banheiro
	private Poltrona poltronas_onibus[];

	public Onibus(String marca, String modelo, String cor, int anofab,
			String dataAquisicao, int qtd, String dataRevisao, boolean tv,
			boolean wifi, boolean wc) throws Exception {
		this.setMarca(marca);
		this.setModelo(modelo);
		this.setCor(cor);
		this.setAnofab(anofab);
		this.setDataAquisicao(dataAquisicao);
		this.setDataRevisao(dataRevisao);
		this.setQtd(qtd);
		this.setTv(tv);
		this.setWifi(wifi);
		this.setWc(wc);
		this.poltronas_onibus = new Poltrona[this.qtd];
		this.initPoltronas();
		/*
		 * 1. faÃ§a a inicializaÃ§Ã£o dos atributos do Ã´nibus 2. acrescente um
		 * atributo que seja um vetor de poltronas this.poltronas_onibus = new
		 * Poltrona [this.qtd_total_poltronas];
		 */
	}

	public void initPoltronas() throws Exception {
		int n = 1;

		for (int i = 0; i < this.poltronas_onibus.length; i++) {
			this.poltronas_onibus[i] = new Poltrona(n, 90, 130, 90);
			n++;
		}
	}

	/*
	 * a quantidade de poltronas (capacidade de poltronas) seraÌ� informada como
	 * paraÌ‚metro. Este meÌ�todo deveraÌ� construir o vetor com esta capacidade
	 * e instanciar todas as poltronas cada uma delas com um nuÌ�mero diferente
	 * (a iniciar de um), sem ocupante e todas elas com as mesmas dimensoÌƒes
	 * (altura, largura e comprimento). Os demais atributos da classe OÌ‚nibus
	 * deveraÌƒo ser nulos.
	 */

	public Onibus(int qtd) throws Exception {
		this.setQtd(qtd);
		this.poltronas_onibus = new Poltrona[this.qtd];
		int n = 1;

		for (int i = 0; i < poltronas_onibus.length; i++) {
			poltronas_onibus[i] = new Poltrona(n, 90, 130, 90);
			n++;
		}

		this.marca = null;
		this.modelo = null;
		this.cor = null;
		this.anofab = 0;
		this.dataAquisicao = null;
		this.dataRevisao = null;
		this.tv = false;
		this.wifi = false;
		this.wc = false;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) throws Exception {
		if (marca == null)
			throw new Exception("Marca invalida");
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) throws Exception {
		if (modelo == null)
			throw new Exception("Modelo invalida");
		this.modelo = modelo;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) throws Exception {
		if (cor == null)
			throw new Exception("Cor invalida");
		this.cor = cor;
	}

	public int getAnofab() {
		return anofab;
	}

	public void setAnofab(int anofab) throws Exception {
		if ((Integer) anofab == null)
			throw new Exception("Ano de fabricacao invalido");
		this.anofab = anofab;
	}

	public int getQtd() {
		return qtd;
	}

	public void setQtd(int qtd) throws Exception {
		if ((Integer) qtd == null)
			throw new Exception("Marca invalida");
		this.qtd = qtd;
	}

	public String getDataAquisicao() {
		return dataAquisicao;
	}

	public void setDataAquisicao(String dataAquisicao) throws Exception {
		if (dataAquisicao == null)
			throw new Exception("Data de aquisicao invalida");
		this.dataAquisicao = dataAquisicao;
	}

	public String getDataRevisao() {
		return dataRevisao;
	}

	public void setDataRevisao(String dataRevisao) throws Exception {
		if (dataRevisao == null)
			throw new Exception("Data de revisao invalida");
		this.dataRevisao = dataRevisao;
	}

	public boolean isTv() {
		return tv;
	}

	public void setTv(boolean tv) throws Exception {
		if ((Boolean) tv == null)
			throw new Exception("TV invalida");
		this.tv = tv;
	}

	public boolean getWifi() {
		return wifi;
	}

	public void setWifi(boolean wifi) throws Exception {
		if ((Boolean) wifi == null)
			throw new Exception("Wifi invalido");
		this.wifi = wifi;
	}

	public boolean getWc() {
		return wc;
	}

	public void setWc(boolean wc) throws Exception {
		if ((Boolean) wc == null)
			throw new Exception("WC invalido");
		this.wc = wc;
	}

	/**
	 * @return the poltronas_onibus
	 */
	public Poltrona[] getPoltronas_onibus() {
		return poltronas_onibus.clone();
	}

	/**
	 * @param poltronas_onibus
	 *            the poltronas_onibus to set
	 */
	public void setPoltronas_onibus(Poltrona[] poltronas_onibus)
			throws Exception {
		if (poltronas_onibus == null)
			throw new Exception("Poltronas invalidas");
		this.poltronas_onibus = poltronas_onibus.clone();
	}

	public int compareTo(Onibus x) {
		if (this.marca == x.marca)
			return 1;
		if (this.modelo == x.modelo)
			return 1;
		if (this.cor == x.cor)
			return 1;
		if (this.anofab == x.anofab)
			return 1;
		if (this.dataAquisicao == x.dataAquisicao)
			return 1;
		if (this.qtd == x.qtd)
			return 1;
		if (this.dataRevisao == x.dataRevisao)
			if (this.tv == x.tv)
				return 1;
		if (this.wifi == x.wifi)
			return 1;
		if (this.wc == x.wc)
			return 1;
		return 0;
	}

	public boolean equals(Onibus x) {
		if (this == x)
			return true;
		if (x == null)
			return false;
		if (x instanceof Onibus)
			if (this.marca == x.marca && this.modelo == x.modelo
					&& this.cor == x.cor && this.anofab == x.anofab
					&& this.dataAquisicao == x.dataAquisicao
					&& this.dataRevisao == x.dataRevisao && this.tv == x.tv
					&& this.wifi == x.wifi && this.wc == x.wc)
				return true;
		return false;
	}

	@Override
	public int hashCode() {
		int resultado = 1;
		resultado = resultado * 7 + this.anofab;
		resultado = resultado * 7 + this.dataAquisicao.hashCode();
		resultado = resultado * 7 + this.dataRevisao.hashCode();
		resultado = resultado * 7 + this.qtd;
		resultado = resultado * 7 + (this.wifi ? 1 : 0);
		resultado = resultado * 7 + (this.tv ? 1 : 0);
		resultado = resultado * 7 + (this.wc ? 1 : 0);
		resultado = resultado * 7 + this.cor.hashCode();
		resultado = resultado * 7 + this.marca.hashCode();
		resultado = resultado * 7 + this.modelo.hashCode();
		return resultado;
	}

	/**
	 * existindo o passageiro, retorna o numero da poltrona caso contrario,
	 * retorna -1
	 */
	public int kd_passageiro(String nome) {
		for (Poltrona poltrona : poltronas_onibus) {
			if(poltrona.getOcupante() == null)
				return -1;
			if (poltrona.getOcupante().getNome().equals(nome)) {
				return poltrona.getNumero();
			}
		}
		return -1;
	}

	/**
	 * havendo poltrona disponi­vel, acrescente o passageiro e retorne TRUE
	 * (faca as perguntas necessarias) caso contrario retorn FALSE
	 */
	public boolean guarde_passageiro() throws Exception {
		for (Poltrona poltrona : poltronas_onibus) {
			if (poltrona.ocupante == null) {
				Passageiro ocp;
				ocp = new Passageiro();
				String nome = JOptionPane.showInputDialog(null,
						"Digite o nome do passageiro: ", "DADOS DO PASSAGEIRO",
						JOptionPane.QUESTION_MESSAGE);
				ocp.setNome(nome);
				String CPF = JOptionPane.showInputDialog(null,
						"Digite o CPF do passageiro: ", "DADOS DO PASSAGEIRO",
						JOptionPane.QUESTION_MESSAGE);
				long CPF2 = Long.parseLong(CPF);
				ocp.setCPF(CPF2);
				String qtd = JOptionPane.showInputDialog(null,
						"Quantidade de viagens do passageiro: ",
						"DADOS DO PASSAGEIRO", JOptionPane.QUESTION_MESSAGE);
				int qtd2 = Integer.parseInt(qtd);
				ocp.setQtd(qtd2);
				if (qtd2 > 0) {
					String prim = JOptionPane
							.showInputDialog(null,
									"Dados da primeira viagem do passageiro: ",
									"DADOS DO PASSAGEIRO",
									JOptionPane.QUESTION_MESSAGE);
					ocp.setPrimeira(prim);
				}
				poltrona.setOcupante(ocp);
				return true;
			}
		}
		return false;

	}

	/**
	 * [Ônibus] Adicionar uma sobrecarga do método guarde_passageiro(), cuja
	 * especificação é: o Assinatura: guarde_passageiro (Passageiro ocp, int
	 * poltronaDesejada):boolean o Funcionamento: Deverá adicionar a instância
	 * de passageiro ocp no lugar desejado (ambos recebidos como parâmetros). o
	 * Retorno: Se a adição do Passageiro for bem sucedida o método deverá
	 * retorna true, senão false (poltrona já ocupada ou se o numero de poltrona
	 * desejada não existe no ônibus).
	 */

	public boolean guarde_passageiro(Passageiro ocp, int poltronaDesejada) {
		for (int i = 0; i < poltronas_onibus.length; i++) {
			if (poltronas_onibus[i].ocupante == null
					&& (1 + i) == poltronaDesejada) {
				poltronas_onibus[i].setOcupante(ocp);
				return true;
			}
		}
		return false;
	}

	/**
	 * [Ônibus] Adicionar uma sobrecarga do método guarde_passageiro(), que
	 * permita ao usuário alocar mais de uma poltrona de uma única vez.
	 * 
	 * @throws Exception
	 */

	public boolean remova(String oq) throws Exception {
		int p;
		if (kd_passageiro(oq) != -1) {
			p = kd_passageiro(oq);
			poltronas_onibus[p].setOcupante(null);
			return true;
		}
		return false;
		/*
		 * removeu retorna TRUE caso contrÃ¡rio FALSE
		 */
	}

	public int onde(String oq) throws Exception {
		for (Poltrona poltrona : poltronas_onibus) {
			if(poltrona.getOcupante() == null) {
				return -1;
			}
			if (poltrona.getOcupante().getNome().equals(oq)) {
				return poltrona.numero;
			}
		}
		return -1;
	}

	/**
	 * Adicionar uma sobrecarga do método onde(String nome), que recebe como
	 * parâmetro um nome e CPF, varre o vetor de poltronas e retorna a instância
	 * de poltrona que está ocupando, ou null caso contrário.
	 * */

	public int onde(String nome, long CPF) {
		for (Poltrona poltrona : poltronas_onibus) {
			if (poltrona.getOcupante().getNome().equals(nome)
					&& poltrona.getOcupante().getCPF() == CPF) {
				return poltrona.getNumero();
			}
		}
		return -1;
	}

	/**
	 * Adicionar um método estaLotado(): boolean (tipo predicado, ou seja que
	 * produz true ou false como retorno) para verificar se o ônibus está ou não
	 * lotado. Este método não receberá parâmetros e se estiver lotado deve
	 * retornar true, senão false.
	 */

	public boolean estaLotado() {
		for (Poltrona poltrona : poltronas_onibus) {
			if (poltrona.getOcupante() == null)
				return false;
		}
		return true;
	}

	public boolean verificaPoltrona(int num) {
		if (poltronas_onibus[num - 1].getOcupante() == null)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return "[ Onibus - Marca: " + marca + "\nModelo: " + modelo + "\nCor: "
				+ cor + "\nAno de fabricacao: " + anofab
				+ "\nData de aquisicao: " + dataAquisicao
				+ "\nQuantidade de poltronas: " + qtd + "\nData de revisao: "
				+ dataRevisao + "\nSe tem TV: " + tv + "\nSe tem Wifi: " + wifi
				+ "\nSe tem banheiro: " + wc + "\nPoltronas: "
				+ poltronas_onibus + " ]";
	}
}