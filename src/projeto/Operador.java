package projeto;

import java.util.List;

public class Operador extends Funcionario {
	
	public Operador(String login, String senha) {
		super(login, senha);
	}
	
	
	public void novoMotorista(List<Motorista> lstMotoristas) throws Exception {
		System.out.println("Funcionario Operador nao tem acesso a esta funcao.");
	}

	public void editaMotorista(List<Motorista> lstMotoristas) throws Exception {
		System.out.println("Funcionario Operador nao tem acesso a esta funcao.");
	}

	public void novoOnibus(List<Onibus> lstOnibus) throws Exception {
		System.out.println("Funcionario Operador nao tem acesso a esta funcao.");
	}

	public void editaOnibus(List <Onibus> lstOnibus) throws Exception {
		System.out.println("Funcionario Operador nao tem acesso a esta funcao.");
	}

	public void novaViagem(List<Viagem> lstViagens, List<Trajeto> lstTrajetos,
			List<Motorista> lstMotoristas, List<Onibus> lstOnibus)
			throws Exception {
		System.out.println("Funcionario Operador nao tem acesso a esta funcao.");
	}

	public void editaViagem(List<Viagem> lstViagens, List<Trajeto> lstTrajetos,
			List<Motorista> lstMotoristas, List<Onibus> lstOnibus) throws Exception {
		System.out.println("Funcionario Operador nao tem acesso a esta funcao.");
	}

	public void novoTrajeto(List<Trajeto> lstTrajetos) throws Exception {
		System.out.println("Funcionario Operador nao tem acesso a esta funcao.");
	}

	public void editaTrajeto(List<Trajeto> lstTrajetos) throws Exception {
		System.out.println("Funcionario Operador nao tem acesso a esta funcao.");
	}

}
