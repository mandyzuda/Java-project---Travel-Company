/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Amanda Barboza Braz RA 14432843

package projeto;

/**
 *
 * @author Amanda B. Braz 14432843
 */
public class Passageiro {

	private String nome;
	private long CPF;
	private int qtd;
	private String primeira;

	public Passageiro(String nome, long CPF, int qtd, String primeira)
			throws Exception {
		this.setNome(nome);
		this.setCPF(CPF);
		this.setQtd(qtd);
		this.setPrimeira(primeira);
	}

	public Passageiro() {
		this.nome = null;
		this.CPF = 0;
		this.qtd = 0;
		this.primeira = null;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) throws Exception {
		if (nome == null)
			throw new Exception("nome invalido");
		this.nome = nome;
	}

	public long getCPF() {
		return CPF;
	}

	public void setCPF(long CPF) throws Exception {
		if ((Long) CPF == null)
			throw new Exception("CPF invalido");
		this.CPF = CPF;
	}

	public int getQtd() {
		return qtd;
	}

	public void setQtd(int qtd) throws Exception {
		if ((Integer) qtd == null)
			throw new Exception("Quantidade de viagens invalido");
		this.qtd = qtd;
	}

	public String getPrimeira() {
		return primeira;
	}

	public void setPrimeira(String primeira) throws Exception {
		if (primeira == null)
			throw new Exception("Primeira viagem invalida");
		this.primeira = primeira;
	}

	@Override
	public String toString() {
		return "Passageiro: [nome=" + nome + ", \nCPF=" + CPF
				+ ", \nquantidade de viagens=" + qtd + ", \nprimeira viagem="
				+ primeira + "]";
	}

	@Override
	public int hashCode() {
		int resultado = 1;
		resultado = resultado * 7 + (int) ((this.CPF >>> 32) | this.CPF);
		;
		resultado = resultado * 7 + this.qtd;
		resultado = resultado * 7 + this.nome.hashCode();
		resultado = resultado * 7 + this.primeira.hashCode();
		return resultado;
	}

	public boolean equals(Object x) {
		if (this == x)
			return true;
		if (x == null)
			return false;
		if (x instanceof Passageiro) {
			Passageiro p = (Passageiro) x;
			if (this.nome == p.nome && this.CPF == p.CPF
					&& this.primeira == p.primeira && this.qtd == p.qtd)
				return true;
		}
		return false;
	}

	/**
	 * Adicione um método que faz a adição de pontos para cada passageiro.
	 * 
	 */

	public void adiciona() {
		this.qtd += 1;
	}

	/**
	 * Adicione um método que retira os pontos de cada passageiro (usado quando
	 * o passageiro usar seus pontos para comprar passagem).
	 */

	public void retira(int pt) {
		this.qtd -= pt;
	}
}
