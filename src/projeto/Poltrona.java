/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Amanda Barboza Braz RA 14432843

package projeto;

import projeto.Passageiro;

/**
 *
 * @author Amanda B. Braz 14432843
 */

public class Poltrona {
	Passageiro ocupante = new Passageiro();
	int numero;
	double largura, altura, comprimento;

	public Poltrona(int numero, double largura, double altura,
			double comprimento) throws Exception {
		this.setNumero(numero);
		this.setLargura(largura);
		this.setAltura(altura);
		this.setComprimento(comprimento);
		ocupante = null;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) throws Exception {
		if ((Integer) numero == null)
			throw new Exception("Numero invalido");
		this.numero = numero;
	}

	public double getLargura() {
		return largura;
	}

	public void setLargura(double largura) throws Exception {
		if ((Double) largura == null)
			throw new Exception("Largura invalido");
		this.largura = largura;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) throws Exception {
		if ((Double) altura == null)
			throw new Exception("Altura invalido");
		this.altura = altura;
	}

	public double getComprimento() {
		return comprimento;
	}

	public void setComprimento(double comprimento) throws Exception {
		if ((Double) comprimento == null)
			throw new Exception("Dia invalido");
		this.comprimento = comprimento;
	}

	public Passageiro getOcupante() {
		return ocupante;
	}

	public void setOcupante(Passageiro ocupante) {
		this.ocupante = ocupante;
	}

	public double calcularAreaUtil() {
		return (largura * comprimento);
	}

	@Override
	public String toString() {
		return "[ Poltrona: numero:" + numero + "\n largura: " + largura
				+ "\n comprimento: " + comprimento + "\n altura: " + altura
				+ "\n ocupante: " + ocupante + "]";
	}

	public int compareTo(Poltrona p) {
		if (this.calcularAreaUtil() < p.calcularAreaUtil())
			return -1;
		if (this.calcularAreaUtil() > p.calcularAreaUtil())
			return 1;
		if (this.getAltura() != p.getAltura())
			return 2;
		if (this.getComprimento() != p.getComprimento())
			return 3;
		if (this.getLargura() != p.getLargura())
			return 4;
		return 0;
	}

	public boolean equals(Poltrona x) {
		if (this == x)
			return true;
		if (x == null)
			return false;
		if (x instanceof Poltrona) {
			Poltrona p = (Poltrona) x;
			if (this.altura == p.altura && this.comprimento == p.comprimento
					&& this.largura == p.largura && this.numero == p.numero
					&& this.ocupante == p.ocupante)
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		int resultado = 1;
		resultado = resultado * 7 + this.numero;
		long x = Double.doubleToLongBits(this.altura);
		resultado = resultado * 7 + (int) ((x >>> 32) | x);
		long y = Double.doubleToLongBits(this.comprimento);
		resultado = resultado * 7 + (int) ((y >>> 32) | y);
		long z = Double.doubleToLongBits(this.largura);
		resultado = resultado * 7 + (int) ((z >>> 32) | z);
		resultado = resultado * 7 + this.ocupante.hashCode();
		return resultado;
	}

	/**
	 * Adicione um método que retorne true ou false para indicar se ela está
	 * sendo ocupada ou não respectivamente.
	 * 
	 * @return se a poltrona está ocupada.
	 */
	public boolean ocupado() {
		if (ocupante == null)
			return false;
		return true;
	}
}
