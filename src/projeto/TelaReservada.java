package projeto;

/**
 * Crie uma Instancia da classe Ônibus com 20 lugares. x Ofereça ao usuário um
 * menu com as seguintes opções: o Visualizar um mapa de assentos:  Deve exibir
 * na tela o Numero de cada uma das poltronas e o nome de seu ocupante, ou então
 * livre se for este o caso. DICA: Use o método toString() de Poltrona para
 * gerar a String com os dados do mapa. o Inserir um passageiro no Ônibus: 
 * Devem se lidos os dados do passageiro (nome e RG) e instanciá-lo (pontos ou
 * milhas = 0).  O programa deve permitir ao passageiro escolher o lugar no
 * qual deseja viajar; Se o passageiro não fizer esta escolha, deverá ser
 * adicionado no próximo lugar livre.  O programador deve, de acordo
 * com a opção acima, cuidar para que o programa acione a sobrecarga apropriada
 * do método adicionarPassageiro(), de acordo com a escolha feita pelo
 * Passageiro.  A chamada não poderá ser feita se a Ônibus já estiver lotado.
 * Oferecer uma consulta pelo nome do passageiro, na qual deve ser impressa a
 * poltrona onde está alocado. Se o passageiro não estiver alocado no Ônibus,
 * imprimir mensagem explicativa.
 *
 *
 * @author Amanda B. Braz 14432843
 */

import java.util.Scanner;

import javax.swing.JOptionPane;

public class TelaReservada {

	public static void main(String[] args) throws Exception {

		Onibus bus = new Onibus(20);
		int op = 1;

		Scanner objLeitor = new Scanner(System.in);

		while (op != 0) {
			System.out.println("\nOP��ES:\n\n\t 1. Visualizar mapa de assentos \n\t 2. Inserir um passageiro no �nibus \n\t 3. Consulta por nome de passageiro\n\t 0. Sair\n\t ");
			op = objLeitor.nextInt();
			switch (op) {
			case 1:
				System.out.println("\n\t[01] [02]\n\t[03] [04]\n\t[05] [06]\n\t[07] [08]\n\t[09] [10]\n\t[11] [12]\n\t[13] [14]\n\t[15] [16]\n\t[17] [18]\n\t[19] [20]");
				for (int i = 0; i < 20; i++) {
					System.out.println("\n" + (bus.getPoltronas_onibus()[i].toString()));
				}
				break;
			case 2:
				if (bus.estaLotado()) {
					System.out.println("\n\tOnibus lotado! N�o ser� poss�vel inserir novo passageiro.");
				}

				else {
					int e;
					System.out.println("Digite a poltrona escolhida pelo passageiro (caso n�o haja escolha, digite 0): ");
					e = objLeitor.nextInt();
					if (e < 1)
						bus.guarde_passageiro();
					else if (e > 20) {
						System.out.println("N�o h� poltronas com este n�mero.");
					} 
					else {
						Passageiro oc;
						oc = new Passageiro();
						String nome = JOptionPane.showInputDialog(null, "Digite o nome do passageiro: ", "DADOS DO PASSAGEIRO", JOptionPane.QUESTION_MESSAGE);
						oc.setNome(nome);
						String CPF = JOptionPane.showInputDialog(null, "Digite o CPF do passageiro: ", "DADOS DO PASSAGEIRO", JOptionPane.QUESTION_MESSAGE);
						long CPF2 = Long.parseLong(CPF);
						oc.setCPF(CPF2);
						String qtd = JOptionPane.showInputDialog(null, "Quantidade de viagens do passageiro: ", "DADOS DO PASSAGEIRO", JOptionPane.QUESTION_MESSAGE);
						int qtd2 = Integer.parseInt(qtd);
						oc.setQtd(qtd2);
						if (qtd2 > 0) {
							String prim = JOptionPane.showInputDialog(null, "Dados da primeira viagem do passageiro: ", "DADOS DO PASSAGEIRO", JOptionPane.QUESTION_MESSAGE);
							oc.setPrimeira(prim);
						}
						bus.guarde_passageiro(oc, e);
					}
				}
				break;
			case 3:
				System.out.println("Digite o nome do passageiro para a busca: ");
				objLeitor.nextLine();
				String nome = objLeitor.nextLine();
				if (bus.onde(nome) != -1)
					System.out.println("Poltrona do passageiro " + nome + ": " + bus.onde(nome));
				else
					System.out.println("O passageiro de nome " + nome + " n�o foi encontrado em nenhuma poltrona do �nibus.");
				break;
			default:
				System.out.println("Op��o inv�lida!");
			}

		}
		objLeitor.close();
	}
}
