/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto;

/**
 *
 * @author Amanda Barboza Braz - RA 14432843
 */

import java.util.Scanner;

import javax.swing.JOptionPane;

public class TesteProjeto {
    //public static void main(String[] args) {
      //  testePoltrona();
    //}
    
  public static void testePassageiro() throws Exception {
        Passageiro cad1, cad2, cad3;
        
        cad1 = new Passageiro();
        cad1.setNome("João Silva");
        cad1.setCPF(25545223518L);
        cad1.setQtd(3);
        cad1.setPrimeira("15/01/2015, 15:21, Campinas-Brasília");
        
        cad2 = new Passageiro();
        cad3 = new Passageiro();
        Scanner objLeitor = new Scanner(System.in);
        
        System.out.println("Digite os dados para cadastrar o novo passageiro:\n\tNome: ");
        cad2.setNome( objLeitor.nextLine() );
        System.out.println("\tCPF: ");
        cad2.setCPF( objLeitor.nextLong() );
        System.out.println("\tQuantidade de viagens realizadas: ");
        cad2.setQtd(objLeitor.nextInt());
        objLeitor.nextLine();
        if(cad2.getQtd()>0)
        {
            System.out.println("\tInformações da primeira viagem (data, horário e idetificação): ");
            cad2.setPrimeira(objLeitor.nextLine());
        }
        System.out.println();
        
        System.out.println("Digite os dados para cadastrar o novo passageiro:\n\tNome: ");
        cad3.setNome(objLeitor.nextLine());
        System.out.println("\tCPF: ");
        cad3.setCPF(objLeitor.nextLong());
        System.out.println("\tQuantidade de viagens realizadas: ");
        cad3.setQtd(objLeitor.nextInt());
        objLeitor.nextLine();
        if(cad3.getQtd()>0)
        {
            System.out.println("\tInformações da primeira viagem (data, horário e idetificação da viagem): ");
            cad3.setPrimeira(objLeitor.nextLine());
        }
        
        System.out.println();
        objLeitor.close();
        

	//printar usando o metodo toString()
	System.out.println (cad1+"\n"+cad2+"\n"+cad3);

	//verificar dois digitados com o mesmo nome
	if(cad2.getNome().equals(cad3.getNome()))
	{
	    System.out.println("\n\tOs dois passageiros digitados possuem o mesmo nome: \n" + cad2.getNome());
	}
	else
	{
	    System.out.println("\n\t Os dois passageiros digitados não possuem os mesmos nomes.\n\n");
	}
	
	//verificar qual dos três possui o nome mais comprido e imprimir seu nome
	if((cad1.getNome().length() > cad2.getNome().length()) && (cad1.getNome().length() > cad3.getNome().length()))
        {
            System.out.println(cad1.getNome() + " tem o nome mais comprido dos três.");
        }
        else if ((cad2.getNome().length() > cad1.getNome().length()) && (cad2.getNome().length() > cad3.getNome().length()))
        {
            System.out.println(cad2.getNome() + " tem o nome mais comprido dos três."); 
        } 
        else
        {
            System.out.println(cad3.getNome() + " tem o nome mais comprido dos três."); 
        }
        
        //Verificar a maior qtd de pontos no programa de milhagem, printar nome e saldo
        
        if((cad1.getQtd() > cad2.getQtd()) && (cad1.getQtd() > cad3.getQtd()))
        {
            System.out.println("O passageiro " + cad1.getNome() + " possui " + cad1.getQtd() + " pontos no programa de milhagem, sendo esta a maior pontuação de todos os passageiros.");
        }
        else if((cad2.getQtd() > cad1.getQtd()) && (cad2.getQtd() > cad3.getQtd()))
        {
            System.out.println("O passageiro " + cad2.getNome() + " possui " + cad2.getQtd() + " pontos no programa de milhagem, sendo esta a maior pontuação de todos os passageiros.");
        }
        else
        {
            System.out.println("O passageiro " + cad3.getNome() + " possui " + cad3.getQtd() + " pontos no programa de milhagem, sendo esta a maior pontuação de todos os passageiros.");
        }
       
        
    }
 
  public static void testePoltrona() throws Exception {
      Poltrona p1, p2, p3;
        
        p1 = new Poltrona(1, 0.5, 0.5, 1.2);
        
        System.out.println("Poltrona 1: \nNúmero: " + p1.getNumero() + "\nÁrea útil: " + p1.calcularAreaUtil());
        
        p2 = new Poltrona(0,0,0,0);
        p3 = new Poltrona(0,0,0,0);
        
        String n = JOptionPane.showInputDialog(null, "Digite o número de identificação: ","Poltrona 2", JOptionPane.QUESTION_MESSAGE );
        int numero = Integer.parseInt(n);
        p2.setNumero(numero);
        
        String alt = JOptionPane.showInputDialog(null, "Digite a altura da poltrona: ", "Poltrona 2", JOptionPane.QUESTION_MESSAGE );
        double altura = Double.parseDouble(alt);
        p2.setAltura(altura);
        
        String larg = JOptionPane.showInputDialog(null, "Digite a largura da poltrona: ", "Poltrona 2", JOptionPane.QUESTION_MESSAGE );
        double largura = Double.parseDouble(larg);
        p2.setLargura(largura);
        
        String comp = JOptionPane.showInputDialog(null, "Digite o comprimento da poltrona: ", "Poltrona 2", JOptionPane.QUESTION_MESSAGE );
        double comprimento = Double.parseDouble(comp);
        p2.setComprimento(comprimento);
        
        n = JOptionPane.showInputDialog(null, "Digite o número de identificação: ","Poltrona 3", JOptionPane.QUESTION_MESSAGE );
        int numero2 = Integer.parseInt(n);
        p3.setNumero(numero2);
        
        alt = JOptionPane.showInputDialog(null, "Digite a altura da poltrona: ", "Poltrona 3", JOptionPane.QUESTION_MESSAGE );
        double altura2 = Double.parseDouble(alt);
        p3.setAltura(altura2);
        
        larg = JOptionPane.showInputDialog(null, "Digite a largura da poltrona: ", "Poltrona 3", JOptionPane.QUESTION_MESSAGE );
        double largura2 = Double.parseDouble(larg);
        p3.setLargura(largura2);
        
        comp = JOptionPane.showInputDialog(null, "Digite o comprimento da poltrona: ", "Poltrona 3", JOptionPane.QUESTION_MESSAGE );
        double comprimento2 = Double.parseDouble(comp);
        p3.setComprimento(comprimento2);
        
        //Verifique se as duas poltronas possuem a mesma área útil e se também possuem as mesmas
        //dimensões (largura, comprimento e altura). Utilize de seus próprios métodos compareTo.
        
        if(p2.compareTo(p3) == 0)
            System.out.println("\n\nPoltrona 2 e Poltrona 3 têm a mesma área útil.");
        if(p2.compareTo(p3) == 2)
            System.out.println("(\n\nPoltrona 2 e Poltrona 3 têm a mesma altura.");
        if(p2.compareTo(p3) == 3)
            System.out.println("\n\nPoltrona 2 e Poltrona 3 têm o mesmo comprimento.");
        if(p2.compareTo(p3) == 4)
            System.out.println("\n\nPoltrona 2 e Poltrona 3 têm a mesma largura.");
        
        // Usando o método setter, modifique a largura e comprimento da poltrona 01 para 0.6 e imprima
    //novamente na tela a identificação e a área útil após a modificação.
        p1.setComprimento(0.6);
        p1.setLargura(0.6);
        System.out.println("Poltrona 1: \nNúmero: " + p1.getNumero() + "\nÁrea útil: " + p1.calcularAreaUtil());
       
      /* Acione o método toString da classe Poltrona e imprima os dados das três instâncias em sequência,
cada um em uma caixa de texto diferente.*/
        JOptionPane.showMessageDialog(null, "Poltronas: \n" + p1 + "\n" + p2 + "\n" + p3, "Poltronas", JOptionPane.INFORMATION_MESSAGE);
        
                
/*- Instancie 2 passageiros (pode copiar o código da classe TestePassageiro do Ex01) e por meio do
método setOcupante(...) defina-os como ocupante de duas das poltronas instanciadas.*/
        Passageiro o2, o3;
        o2 = new Passageiro();
        String nome = JOptionPane.showInputDialog(null, "Digite o nome do passageiro: ", "DADOS DO PASSAGEIRO", JOptionPane.QUESTION_MESSAGE);
        o2.setNome(nome);
        String CPF = JOptionPane.showInputDialog(null, "Digite o CPF do passageiro: ", "DADOS DO PASSAGEIRO", JOptionPane.QUESTION_MESSAGE);
        long CPF2 = Long.parseLong(CPF);
        o2.setCPF(CPF2);
        String qtd = JOptionPane.showInputDialog(null, "Quantidade de viagens do passageiro: ", "DADOS DO PASSAGEIRO", JOptionPane.QUESTION_MESSAGE);
        int qtd2 = Integer.parseInt(qtd);
        o2.setQtd(qtd2);
        if(qtd2>0)
        {
            String prim = JOptionPane.showInputDialog(null, "Dados da primeira viagem do passageiro: ", "DADOS DO PASSAGEIRO", JOptionPane.QUESTION_MESSAGE);
            o2.setPrimeira(prim);
        }
        p2.setOcupante(o2);
        
        o3 = new Passageiro();
        nome = JOptionPane.showInputDialog(null, "Digite o nome do passageiro: ", "DADOS DO PASSAGEIRO", JOptionPane.QUESTION_MESSAGE);
        o3.setNome(nome);
        CPF = JOptionPane.showInputDialog(null, "Digite o CPF do passageiro: ", "DADOS DO PASSAGEIRO", JOptionPane.QUESTION_MESSAGE);
        CPF2 = Long.parseLong(CPF);
        o3.setCPF(CPF2);
        qtd = JOptionPane.showInputDialog(null, "Quantidade de viagens do passageiro: ", "DADOS DO PASSAGEIRO", JOptionPane.QUESTION_MESSAGE);
        qtd2 = Integer.parseInt(qtd);
        o3.setQtd(qtd2);
        if(qtd2>0)
        {
            String prim = JOptionPane.showInputDialog(null, "Dados da primeira viagem do passageiro: ", "DADOS DO PASSAGEIRO", JOptionPane.QUESTION_MESSAGE);
            o3.setPrimeira(prim);
        }
        
        p3.setOcupante(o3);
/*- Imprima os dados das três poltronas que foram instanciadas e o nome do ocupante de cada uma
delas. Cuidado, pois se a poltrona não tiver um ocupante, deverá ser impresso LIVRE.  */
        if(p1.getOcupante() == null)
            System.out.println("\n\n\n\tPoltrona 1: LIVRE\n" + p1);
        else
            System.out.println("\n\n\n\tPoltrona 1: \n" + p1);
        if(p2.getOcupante() == null)
            System.out.println("\n\n\n\tPoltrona 2: LIVRE\n" + p2);
        else
            System.out.println("\n\n\n\tPoltrona 2: \n" + p2);
        if(p3.getOcupante() == null)
            System.out.println("\n\n\n\tPoltrona 3: LIVRE\n" + p3);
        else
            System.out.println("\n\n\n\tPoltrona 3: \n" + p3);


  }
}

  
