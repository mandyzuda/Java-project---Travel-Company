package projeto;

public class Trajeto {
	private String cidOrigem;
	private String cidDestino;
	private String[] cidIntermed;
	private float dist;
	private float tempo;
	private int qtdPedagios;
	private float totalPedagios;

	public Trajeto(String cidO, String cidD, float dist, float tempo, int qtdP,
			float totalP) throws Exception {
		this.setCidDestino(cidD);
		this.setCidOrigem(cidO);
		this.setDist(dist);
		this.setTempo(tempo);
		this.setQtdPedagios(qtdP);
		this.setTotalPedagios(totalP);
		this.cidIntermed = new String[10];
	}

	public Trajeto() {
		this.cidDestino = null;
		this.cidOrigem = null;
		this.dist = 0;
		this.tempo = 0;
		this.qtdPedagios = 0;
		this.totalPedagios = 0;
		this.cidIntermed = new String[10];
	}

	public Trajeto(Trajeto t) {
		this.cidDestino = t.cidDestino;
		this.cidOrigem = t.cidOrigem;
		this.dist = t.dist;
		this.qtdPedagios = t.qtdPedagios;
		this.tempo = t.tempo;
		this.totalPedagios = t.totalPedagios;
		this.cidIntermed = new String[t.cidIntermed.length];
		for (int i = 0; i < t.cidIntermed.length; i++) {
			this.cidIntermed[i] = t.cidIntermed[i];
		}
	}

	public Object clone() {
		return new Trajeto(this);
	}

	public String getCidOrigem() {
		return cidOrigem;
	}

	public void setCidOrigem(String cidOrigem) throws Exception {
		if (cidOrigem == null)
			throw new Exception("Cidade origem invalido");
		this.cidOrigem = cidOrigem;
	}

	public String getCidDestino() {
		return cidDestino;
	}

	public void setCidDestino(String cidDestino) throws Exception {
		if (cidDestino == null)
			throw new Exception("Cidade destino invalido");
		this.cidDestino = cidDestino;
	}

	public String[] getCidIntermed() {
		return cidIntermed;
	}

	public void setCidIntermed(String[] cidIntermed) throws Exception {
		if (cidIntermed == null)
			throw new Exception("Cidades intermediarias invalidas");
		this.cidIntermed = cidIntermed;
	}

	public float getDist() {
		return dist;
	}

	public void setDist(float dist) throws Exception {
		if ((Float) dist == null)
			throw new Exception("Distancia invalido");
		this.dist = dist;
	}

	public float getTempo() {
		return tempo;
	}

	public void setTempo(float tempo) throws Exception {
		if ((Float) tempo == null)
			throw new Exception("Tempo invalido");
		this.tempo = tempo;
	}

	public int getQtdPedagios() {
		return qtdPedagios;
	}

	public void setQtdPedagios(int qtdPedagios) throws Exception {
		if ((Integer) qtdPedagios == null)
			throw new Exception("Quantidade de pedagios invalido");
		this.qtdPedagios = qtdPedagios;
	}

	public float getTotalPedagios() {
		return totalPedagios;
	}

	public void setTotalPedagios(float totalPedagios) throws Exception {
		if ((Float) totalPedagios == null)
			throw new Exception("Total de pedagios invalido");
		this.totalPedagios = totalPedagios;
	}

	@Override
	public int hashCode() {
		int resultado = 1;
		resultado = resultado * 7 + this.cidOrigem.hashCode();
		resultado = resultado * 7 + this.cidDestino.hashCode();
		resultado = resultado * 7 + this.cidIntermed.hashCode();
		resultado = resultado * 7 + this.qtdPedagios;
		resultado = resultado * 7 + Float.floatToIntBits(this.dist);
		resultado = resultado * 7 + Float.floatToIntBits(this.tempo);
		resultado = resultado * 7 + Float.floatToIntBits(this.totalPedagios);
		return resultado;
	}

	@Override
	public boolean equals(Object x) {
		if (this == x)
			return true;

		if (x == null)
			return false;

		if (x instanceof Trajeto) {
			Trajeto h = (Trajeto) x;

			if (this.cidDestino == h.cidDestino
					&& this.cidIntermed == h.cidIntermed
					&& this.cidOrigem == h.cidOrigem
					&& this.qtdPedagios == h.qtdPedagios
					&& this.totalPedagios == h.totalPedagios
					&& this.dist == h.dist && this.tempo == h.tempo)
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "[ Trajeto - Cidade Origem: " + cidOrigem
				+ " \nCidade Intermediarias: " + cidIntermed
				+ " \nCidade Destino: " + cidDestino + " \nTempo estimado: "
				+ tempo + " \nDistancia: " + dist + "\nQuantidade de Pedagios: "
				+ qtdPedagios + " \nValor Total de Pedagios: " + totalPedagios
				+ "]";
	}

}
