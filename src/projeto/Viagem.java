package projeto;

public class Viagem {
	private String id;
	private Onibus bus;
	private Trajeto traj;
	private Motorista mot;
	private float tarifa;
	private int dia;
	private int mes;
	private int ano;
	private int hora;
	private int minuto;
	private int qtdPoltVazia;
	private int qtdPoltPreench;

	public Viagem(String id, Onibus bus, Trajeto traj, Motorista mot,
			float tarifa, int dia, int mes, int ano, int hora, int minuto,
			int qtdPV, int qtdPP) throws Exception {
		this.setId(id);
		this.setBus(bus);
		this.setTraj(traj);
		this.setMot(mot);
		this.setTarifa(tarifa);
		this.setDia(dia);
		this.setMes(mes);
		this.setAno(ano);
		this.setHora(hora);
		this.setMinuto(minuto);
		this.setQtdPoltVazia(qtdPV);
		this.setQtdPoltPreench(qtdPP);
	}

	public Viagem(Viagem v) {
		this.id = v.id;
		this.bus = v.bus;
		this.traj = v.traj;
		this.mot = v.mot;
		this.tarifa = v.tarifa;
		this.dia = v.dia;
		this.mes = v.mes;
		this.ano = v.ano;
		this.hora = v.hora;
		this.minuto = v.minuto;
		this.qtdPoltVazia = v.qtdPoltVazia;
		this.qtdPoltPreench = v.qtdPoltPreench;
	}

	public Viagem() {
		this.id = null;
		this.bus = null;
		this.traj = null;
		this.mot = null;
		this.tarifa = 0;
		this.dia = 0;
		this.mes = 0;
		this.ano = 0;
		this.hora = 0;
		this.minuto = 0;
		this.qtdPoltVazia = 0;
		this.qtdPoltPreench = 0;
	}

	public Object clone() {
		return new Viagem(this);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) throws Exception {
		if (id == null)
			throw new Exception("Id invalido");
		this.id = id;
	}

	public Onibus getBus() {
		return bus;
	}

	public void setBus(Onibus bus) throws Exception {
		if (bus == null)
			throw new Exception("Onibus invalido");
		this.bus = bus;
	}

	public Trajeto getTraj() {
		return traj;
	}

	public void setTraj(Trajeto traj) throws Exception {
		if (traj == null)
			throw new Exception("Trajeto invalido");
		this.traj = traj;
	}

	public Motorista getMot() {
		return mot;
	}

	public void setMot(Motorista mot) throws Exception {
		if (mot == null)
			throw new Exception("Motorista invalido");
		this.mot = mot;
	}

	public float getTarifa() {
		return tarifa;
	}

	public void setTarifa(float tarifa) throws Exception {
		if ((Float) tarifa == null)
			throw new Exception("Tarifa invalido");
		this.tarifa = tarifa;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) throws Exception {
		if ((Integer) dia == null)
			throw new Exception("Dia invalido");
		this.dia = dia;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) throws Exception {
		if ((Integer) mes == null)
			throw new Exception("Mes invalido");
		this.mes = mes;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) throws Exception {
		if ((Integer) ano == null)
			throw new Exception("Ano invalido");
		this.ano = ano;
	}

	public int getHora() {
		return hora;
	}

	public void setHora(int hora) throws Exception {
		if ((Integer) hora == null)
			throw new Exception("Hora invalido");
		this.hora = hora;
	}

	public int getMinuto() {
		return minuto;
	}

	public void setMinuto(int minuto) throws Exception {
		if ((Integer) minuto == null)
			throw new Exception("Minuto invalido");
		this.minuto = minuto;
	}

	public int getQtdPoltVazia() {
		return qtdPoltVazia;
	}

	public void setQtdPoltVazia(int qtdPoltVazia) throws Exception {
		if ((Integer) qtdPoltVazia == null)
			throw new Exception("Quantidade de poltronas vazias invalido");
		this.qtdPoltVazia = qtdPoltVazia;
	}

	public int getQtdPoltPreench() {
		return qtdPoltPreench;
	}

	public void setQtdPoltPreench(int qtdPoltPreench) throws Exception {
		if ((Integer) qtdPoltPreench == null)
			throw new Exception("Quantidade de poltronas preenchidas invalido");
		this.qtdPoltPreench = qtdPoltPreench;
	}

	@Override
	public int hashCode() {
		int resultado = 1;
		resultado = resultado * 7 + this.id.hashCode();
		resultado = resultado * 7 + this.hora;
		resultado = resultado * 7 + this.minuto;
		resultado = resultado * 7 + this.bus.hashCode();
		resultado = resultado * 7 + this.traj.hashCode();
		resultado = resultado * 7 + this.mot.hashCode();
		resultado = resultado * 7 + this.qtdPoltVazia;
		resultado = resultado * 7 + this.qtdPoltPreench;
		resultado = resultado * 7 + this.dia;
		resultado = resultado * 7 + this.mes;
		resultado = resultado * 7 + this.ano;
		return resultado;
	}

	@Override
	public boolean equals(Object x) {
		if (this == x)
			return true;

		if (x == null)
			return false;

		if (x instanceof Viagem) {
			Viagem v = (Viagem) x;

			if (this.id == v.id && this.hora == v.hora
					&& this.minuto == v.minuto && this.bus == v.bus
					&& this.traj == v.traj && this.mot == v.mot
					&& this.qtdPoltVazia == v.qtdPoltVazia
					&& this.qtdPoltPreench == v.qtdPoltPreench
					&& this.dia == v.dia && this.mes == v.mes
					&& this.ano == v.ano)
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "[ Viagem - Identificao: " + id + "\nOnibus que fara a viagem: "
				+ bus + "\nTrajeto da viagem: " + traj
				+ "\nMotorista da viagem: " + mot + "\nPreco da tarifa: "
				+ tarifa + "\nData da partida: " + dia + "/" + mes + "/" + ano
				+ "\nHorario da Partida: " + hora + ":" + minuto
				+ "\nQuantidade de poltronas vazias: " + qtdPoltVazia
				+ "\nQuantidade de poltronas preenchidas: " + qtdPoltPreench
				+ " ]";
	}

}
